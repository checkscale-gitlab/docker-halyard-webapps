#!/bin/sh

DIR=$(dirname "${0}")

. "${DIR}/hbase-entrypoint-helpers.sh"

set_zoo_quorum
set_root_dir

. "${DIR}/hbase-set-props.sh"
. "${DIR}/hadoop-set-props.sh"

wait_for

# https://merck.github.io/Halyard/tools.html#rdf4j-web-applications
CLASSPATH="$(${HADOOP_HOME}/bin/hadoop classpath):$(HBASE_DISABLE_HADOOP_CLASSPATH_LOOKUP=true ${HBASE_HOME}/bin/hbase classpath)"
for I in $(echo "${CLASSPATH}" | tr ':' ' '); do
	case "${I}" in
	*.jar)	JETTY_ARGS="${JETTY_ARGS} --jar ${I}"
		;;
	*/\*)	JETTY_ARGS="${JETTY_ARGS} --lib ${I##*/}"
		;;
	*)	JETTY_ARGS="${JETTY_ARGS} --classes ${I}"
	esac
done
# https://www.eclipse.org/jetty/documentation/current/runner.html
JETTY_ARGS="${JETTY_ARGS} --port ${JETTY_PORT:-80}"
for I in ${HALYARD_HOME}/*.war; do
	WEB_PATH=$(basename ${I} .war)
	JETTY_ARGS="${JETTY_ARGS} --path ${WEB_PATH} ${I}"
done

exec su "${JETTY_USER}" -c "exec ${JAVA_HOME}/bin/java -jar ${JETTY_HOME}/jetty-runner.jar ${JETTY_ARGS} ${@}"
